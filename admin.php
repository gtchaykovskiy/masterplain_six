<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $user = 'u16737';
    $pass = '1949424';
    $db = new PDO('mysql:host=localhost;dbname=u16737', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.PDOStatement::execute().
    try {
        $stmt = $db->prepare("SELECT login FROM admin_db");
        $stmt->execute();
        $values['login'] = $stmt->fetchColumn();

        $stmt = $db->prepare("SELECT password FROM admin_db");
        $stmt->execute();
        $values['password'] = $stmt->fetchColumn();
        }
    catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    if (empty($_SERVER['PHP_AUTH_USER']) ||
        empty($_SERVER['PHP_AUTH_PW']) ||
        $_SERVER['PHP_AUTH_USER'] != $values['login'] ||
        md5($_SERVER['PHP_AUTH_PW']) != md5($values['password'])) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="My site"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }

    print('Вы успешно авторизовались и видите защищенные паролем данные.');
    $connect_to_db = mysqli_connect('localhost', 'u16737', '1949424', 'u16737');

        $qr_result = mysqli_query($connect_to_db,"SELECT * FROM georg_db");
        $rows = mysqli_fetch_array($qr_result);


    echo '<table cellpadding="4" cellspacing="0" border="1">';
        echo  ' <tbody>';
        echo'<tr>';
            echo ' <th>ID</th>';
            echo '<th>ФИО</th>';
            echo ' <th>Email</th>';
            echo ' <th>Дата рождения</th>';
            echo '<th>Гендер</th>';
            echo '<th>Количество конечностей</th>';
            echo '<th>Сверхспособности</th>';
            echo '<th>Биография</th>';
            echo ' <th>Логин</th>';
            echo ' <th>Пароль</th>';
            echo '</tr>';

    while($data = mysqli_fetch_array($qr_result)){
        echo '<tr>';
        echo '<td>' . $data['id'] . '</td>';
        echo '<td>' . $data['fio'] . '</td>';
        echo '<td>' . $data['email'] . '</td>';
        echo '<td>' . $data['year'] . '</td>';
        echo '<td>' . $data['sex'] . '</td>';
        echo '<td>' . $data['kon'] . '</td>';
        echo '<td>' . $data['ability'] . '</td>';
        echo '<td>' . $data['text1'] . '</td>';
        echo '<td>' . $data['login'] . '</td>';
        echo '<td>' . $data['password'] . '</td>';

        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
 ?>

    <title>Admin panel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <form actiom="" method="post"><br>
    <input type="text" name="id" placeholder="Введите id пользователя"><br>

        <button class="btn btn-success pull-right" name="submit">Удалить</button><br>
    </form>
<?php
}

else if (isset($_POST['submit'])){
    $con = mysqli_connect('localhost', 'u16737', '1949424', 'u16737');
    mysqli_query($con,"DELETE FROM `georg_db` WHERE `georg_db`. `id` = ".$_POST['id']);

    header('Location: admin.php');

}
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// ********