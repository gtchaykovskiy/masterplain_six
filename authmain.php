<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<h1>Форма авторизации</h1>
<form action="" method="post">
  <input name="login" placeholder="login" /><br>
  <input name="password" type="password" placeholder="pass" /><br>
  <button class="btn btn-success" type="submit" value="Войти">Войти </button><br>
</form>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.

    $user = 'u16737';
    $pass = '1949424';
    $db = new PDO('mysql:host=localhost;dbname=u16737', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    // Подготовленный запрос. Не именованные метки.PDOStatement::execute().
    try {
        $stmt = $db->prepare("SELECT password FROM georg_db WHERE login = ?");
        $stmt -> execute(array($_POST['login']));
          $data = $stmt->fetchAll();

        if (md5($_POST['password']) == $data[0]){
            // Если все ок, то авторизуем пользователя.
            $_SESSION['login'] = $_POST['login'];
            // Делаем перенаправление.
            header('Location: ./');
        }
        else {
          // code...
          header('Location: ./');
        }
      }
        catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    }
