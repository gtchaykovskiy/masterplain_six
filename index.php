<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_COOKIE['loc'])) {
     $messages['save'] = '<div>Имя уже занято</div>';
 }
  if (!empty($_COOKIE['save'])) {
    //Удаляем куку, указывая время в прошлом.
    setcookie('save', '', 100000);
    //setcookie('exist', '', 100000);
    setcookie('login', '', 100000);
    setcookie('password', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    ?>
    <script> alert('Результаты сохранены.');</script>
      <?php
    //Если в куках имеется пароль, то выводим сообщение
    if (!empty($_COOKIE['password'])) {
      $messages[] = sprintf('Вы можите <a href="authmain.php">войти</a> с логином <strong>%s</strong> и паролем <strong>%s</strong> для изменения данных.',
      strip_tags($_COOKIE['login']), strip_tags($_COOKIE['password']));
    }
  }

  //Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['kon'] = !empty($_COOKIE['kon_error']);
  $errors['ability'] = !empty($_COOKIE['ability_error']);
  $errors['text1'] = !empty($_COOKIE['text1_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

  //Выдаем сообщения об ошибках
  if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="">Заполните Имя.</div>';
    }
  if ($errors['email']){
    //удаляем куку, указывая время в прошлом.
    setcookie('email_error', '', 100000);
    //Выводим сообщение
    $messages[] = '<div class="error">Заполните почту.</div>';
  }
  if ($errors['year']){
    //удаляем куку, указывая время в прошлом.
    setcookie('year_error', '', 100000);
    //Выводим сообщение
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['sex']){
    //удаляем куку, указывая время в прошлом.
    setcookie('sex_error', '', 100000);
    //Выводим сообщение
    $messages[] = '<div class="error">Заполните пол.</div>';
  }
  if ($errors['kon']){
    //удаляем куку, указывая время в прошлом.
    setcookie('kon_error', '', 100000);
    //Выводим сообщение
    $messages[] = '<div class="error">Выберите кол-во конечностей.</div>';
  }
  if ($errors['ability']){
    //удаляем куку, указывая время в прошлом.
    setcookie('ability_error', '', 100000);
    //Выводим сообщение
    $messages[] = '<div class="error">Выберите способность.</div>';
  }
  if ($errors['text1']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('text1_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="">Заполните биографию.</div>';
    }
  if ($errors['checkbox']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('checkbox_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Выберите это поле.</div>';
    }



  //Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['year'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['kon'] = empty($_COOKIE['kon_value']) ? '' : strip_tags($_COOKIE['kon_value']);
  $values['ability'] = empty($_COOKIE['ability_value']) ? '' : strip_tags($_COOKIE['ability_value']);
  $values['text1'] = empty($_COOKIE['text1_value']) ? '' : strip_tags($_COOKIE['text1_value']);
    //Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и раннее в сессию записан факт успешного логина
    if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
        $user = 'u16737';
        $pass = '1949424';
        $db = new PDO('mysql:host=localhost;dbname=u16737', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    try {
      //Подготавливаем SQL запрос в базу
      //Санитайзеры стр 664
            $stmt = $db->prepare("SELECT fio FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['fio'] = strip_tags($stmt->fetchColumn());

            $stmt = $db->prepare("SELECT email FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['email'] = strip_tags($stmt->fetchColumn());

            $stmt = $db->prepare("SELECT year FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['year'] = strip_tags( $stmt->fetchColumn());

            $stmt = $db->prepare("SELECT sex FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['sex'] = strip_tags($stmt->fetchColumn());

            $stmt = $db->prepare("SELECT kon FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['kon'] = strip_tags( $stmt->fetchColumn());

            $stmt = $db->prepare("SELECT ability FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['ability'] = strip_tags( $stmt->fetchColumn());

            $stmt = $db->prepare("SELECT text1 FROM georg_db WHERE login = ?");
            $stmt->execute([$_SESSION['login']]);
            $values['text1'] = strip_tags($stmt->fetchColumn());

            $values['checkbox'] = true;

    }
    catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
        }
        $messages[] ='Вход с логином ';
    $messages[] = $_SESSION['login'];
  }
  // Включаем содержимое файла form.php.
  include('work_space.php');
  // Завершаем работу скрипта.
//  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
// Проверяем ошибки.
$errors = FALSE;
    if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }

if (empty($_POST['email'])) {
setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  //Сохраняем на месяц
  setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['year'])) {
  //Выдфем куку на день с флажком об ошибке в поле fio.
  setcookie('year_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  //Сохраняем на месяц
  setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
}

if (empty($_POST['sex'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
    }
if (empty($_POST['kon'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('kon_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('kon_value', $_POST['kon'], time() + 30 * 24 * 60 * 60);
    }
//Сохраняем на месяц


if (empty($_POST['text1'])) {
  //Выдфем куку на день с флажком об ошибке в поле fio.
  setcookie('text1_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else{
//Сохраняем на месяц
setcookie('text1_value', $_POST['text1'], time() + 30 * 24 * 60 * 60);
}
setcookie('ability_value', serialize($_POST['ability']), time() + 30 * 24 * 60 * 60);
if (empty($_POST['checkbox'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('checkbox_value', $_POST['checkbox'], time() + 30 * 24 * 60 * 60);
    }

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************


switch($_POST['sex']) {
        case 'm': {
            $sex='m';
            break;
        }
        case 'f':{
            $sex='f';
            break;
        }
    };

    switch($_POST['kon']) {
        case '1': {
            $kon='1';
            break;
        }
        case '2':{
            $kon='2';
            break;
        }
        case '3':{
            $kon='3';
            break;
        }
        case '4':{
            $kon='4';
            break;
        }
    };

    $ability = serialize($_POST['ability']);
    if ($errors) {
      // При наличии ошибок завершаем работу скрипта.
      header('Location: index.php');
      exit();
    }
    else {
            // Удаляем Cookies с признаками ошибок.
            setcookie('fio_error', '', 100000);
            setcookie('email_error', '', 100000);
            setcookie('year_error', '', 100000);
            setcookie('sex_error', '', 100000);
            setcookie('kon_error', '', 100000);
            setcookie('text1_error', '', 100000);
            setcookie('checkbox_error', '', 100000);
            setcookie('ability_error', '', 100000);
        }
          $user = 'u16737';
          $pass = '1949424';
          $db = new PDO('mysql:host=localhost;dbname=u16737', $user, $pass,  array(PDO::ATTR_PERSISTENT => true));
        // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])) {
          // TODO: перезаписать данные в БД новыми данными,
          // кроме логина и пароля.
          try {

      $stmt = $db->prepare("UPDATE georg_db SET fio = ?, email = ?, year = ?, sex = ?, kon = ?, ability = ?, text1 = ? WHERE login = {$_SESSION['login']}");
      $stmt->execute(array($_POST['fio'],$_POST['email'],$_POST['year'],$sex,$kon,$ability,$_POST['text1']));

             }
             catch(PDOException $e){
                 print('Error : ' . $e->getMessage());
                 exit();
             }

    }
    else {
      function nickname_gen() {
    $symbol_arr = array('aeiouy', 'bcdfghjklmnpqrstvwxz');
    $length = mt_rand(5, 8);
    $return = array();
    foreach ($symbol_arr as $k => $v)
        $symbol_arr[$k] = str_split($v);
    for ($i = 0; $i < $length; $i++) {
        while (true) {
            $symbol_x = mt_rand(0, sizeof($symbol_arr) - 1);
            $symbol_y = mt_rand(0, sizeof($symbol_arr[$symbol_x]) - 1);
            if ($i > 0 && in_array($return[$i - 1], $symbol_arr[$symbol_x]))
                continue;
            $return[] = $symbol_arr[$symbol_x][$symbol_y];
            break;
        }
    }
    $return = ucfirst(implode('', $return));
    return $return;
}
function generatePassword($length = 8)
{
    $chars = str_shuffle('abdefhiknrstyzABDEFGHKNQRSTYZ1234567890');
    return substr($chars, 0, $length);
}
      // Генерируем уникальный логин и пароль.
      // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
      $login = nickname_gen();
      $password = generatePassword();
      // Сохраняем в Cookies.
      setcookie('login', $login);
      setcookie('password', $password);

      $_POST['login'] = $login;
      $_POST['pass'] = $password;

      // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
      //$password_new = md5($password);
      // Подготовленный запрос. Не именованные метки.
      try {
          $stmt = $db->prepare("INSERT INTO georg_db SET fio = ?, email = ?, year = ?, sex = ?, kon = ?, ability = ?, text1 = ?,login = ?, password = ?");
          $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['year'],$sex,$kon,$ability,$_POST['text1'],$login,md5($password)));
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
}
        // Сохраняем куку с признаком успешного сохранения.
        setcookie('save', '1');

        // Делаем перенаправление.
        header('Location: ./');
}
