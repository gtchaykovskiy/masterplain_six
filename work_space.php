<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="style.css" rel="stylesheet">
  <title>6-x Task</title>
</head>
<body>
  <button class="btn btn-success pull-right" name="submit" onclick="location.replace('authmain.php');">Войти</button><br><br>


  <button class="btn btn-success pull-right" name="submit" onclick="location.replace('admin.php');">Админ панелька</button><br>
  <?php
  if (!empty($messages)) {
    print('<div id="messages">');
    //Выводим все сообщения.
    foreach ($messages as $message) {
      print($message);
      // code...
    }
    print('</div>');
  }
   ?>
  <h1 font-size="40px">6 Task</h1>
    <div class="form" id="action_form">
    <h2>Форма</h2>
   <form action="" method="post">
     <div class="form-inner">
     <label>

      <h4> Введите имя:</h4>
       <input class="form-control" type="text" name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="Введите ваше Ф.И.О" required>
     </label><br />
   </div>
   <label><h4>E-mail</h4></label></p>
   <label><input class="form-control" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email"></label>
     <label>
       <label><h4>Дата рождения</h4>
       <input name="year" type="date" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>">
     <div>
       </label>
         Выберете пол <br>
         <label>
             <input type="radio" value="m" name="sex" <?php if ($values['sex']=="m") {print 'checked';} ?>> муж
         </label>
         <label>
             <input type="radio" value="f" name="sex" <?php if ($values['sex']=="f") {print 'checked';} ?>>жен
         </label>
     </div>
<div>
         Кол-во конечностей <br>
         <label>
             <input type="radio" value="1" name="kon" <?php if ($errors['kon']) {print 'class="error"';} ?> <?php if ($values['kon']=="1") {print 'checked';} ?>>1
         </label>
         <label>
             <input type="radio" value="2" name="kon" <?php if ($errors['kon']) {print 'class="error"';} ?> <?php if ($values['kon']=="2") {print 'checked';} ?>>2
         </label>
         <label>
             <input type="radio" value="3" name="kon" <?php if ($errors['kon']) {print 'class="error"';} ?> <?php if ($values['kon']=="3") {print 'checked';} ?>>3
         </label>
         <label>
             <input type="radio" value="4" name="kon" <?php if ($errors['kon']) {print 'class="error"';} ?> <?php if ($values['kon']=="4") {print 'checked';} ?>>4
         </label>
     </div>
   <label>
     Сверхспособности:
     <br />
     <select class="form-control" name="ability[]" multiple="multiple">
       <option  value="one" <?php if (strpos($values['ability'], "one")) {print 'selected';} ?>>Бессертие</option>
       <option value="two" <?php if (strpos($values['ability'], "two")) {print 'selected';} ?>>Прохождение сквозь стены</option>
       <option value="three" <?php if (strpos($values['ability'], "three")) {print 'selected';} ?>>Левитация</option>
     </select>
   </label><br />
   <br> Биография<br>
     <textarea class="form-control" name="text1" cols="10" rows="10" <?php if ($errors['text1']) {print 'class="error"';} ?>><?php print $values['text1']; ?></textarea>
     <br>
     <label <?php if ($errors['checkbox']) {print 'class="error"';} ?>>
         <input type="checkbox" name="checkbox">
         С контрактом ознакомлен
     </label>
     <br>
     <input  type="submit" value="Отправить">
   </div>
   </form>
 </div>
</body>
</html>
